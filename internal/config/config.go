package config

import (
	"gopkg.in/yaml.v3"
	"io/ioutil"
)

type Config struct {
	Concurrent int   `yaml:"concurrent"`
	Timeout    int64 `yaml:"timeout"`
}

func NewConfig(path string) *Config {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	var config Config
	if err = yaml.Unmarshal(file, &config); err != nil {
		panic(err)
	}

	return &config
}
