package runner

import (
	"errors"
	"github.com/google/uuid"
	"gitlab.com/divilla/exporter/internal/config"
	"go.uber.org/zap"
	"math/rand"
	"time"
)

type (
	Result struct {
		Id      uuid.UUID
		Start   time.Time
		Success bool
		Err     error
	}
)

func Run(cfg *config.Config, logger *zap.SugaredLogger) {
	// calculated timeout in seconds from config
	timeout := time.Duration(cfg.Timeout) * time.Second

	// chConcurrent is buffered channel used to limit max number of concurrent goroutines
	chConcurrent := make(chan struct{}, cfg.Concurrent)

	// chResult is channel used for sending task Result to
	chResult := make(chan Result)

	// resultHandler is goroutine used to handle result of performed task
	// it handles **success** & **error** results (**timeout** is treated as error)
	go resultHandler(timeout, chConcurrent, chResult, logger)

	for {
		// use chConcurrent channel to claim one goroutine and start the task
		chConcurrent <- struct{}{}
		id := uuid.New()
		start := time.Now()

		// chDone is buffered channel generated for each "task" goroutine
		// in order to prevent memory leaks it needs to be closed and "drained"
		chDone := make(chan struct{}, 1)

		go processTask(id, start, chConcurrent, chDone, chResult)
		go timeoutResult(id, start, timeout, chDone, chResult)
	}
}

// processTask is a function, executed as goroutine, utilized to perform entire DB logic
func processTask(id uuid.UUID, start time.Time, chConcurrent <-chan struct{}, chDone chan struct{}, chResult chan<- Result) {
	// *** All db related tasks & logic should be here

	// isTimeout() should be checked before execution of each DB command
	// **Important:** one cannot rely on isTimeout, because timeout could happen in next processor tick
	// therefor all DB commands should be written in a way that avoids error resulting from change of "id"
	if isTimeout(chDone) {
		return
	}

	// randomly set duration for **timeout** testing purposes --- this should be removed
	time.Sleep(time.Duration(rand.Int63n(12)) * time.Millisecond * 100)

	// randomly generate error for testing purposes --- this should be removed
	rnd := rand.Int31n(9)
	if rnd == 0 {
		// each time you encounter error this two lines should be called
		errorResult(id, start, errors.New("error: "+id.String()), chDone, chResult)
		return
	}

	select {
	// writing into Done channel will prevent further writing into chResult (disable **timeout** result)
	case chDone <- struct{}{}:
		// send **success** to resultHandler
		chResult <- Result{
			Id:      id,
			Start:   start,
			Success: true,
		}
		// this line is here to unblock iterator at line nr. 35 and enable processing of the new task
		<-chConcurrent

	// chDone is buffered channel generated for each "task" goroutine
	// in order to prevent memory leaks it needs to be closed and "drained"
	default:
		close(chDone)
		<-chDone
		chDone = nil
	}
}

// isTimeout should be checked before each database command and end function if it returns true
func isTimeout(chDone chan struct{}) bool {
	select {
	case <-chDone:
		close(chDone)
		chDone = nil
		return true
	default:
		return false
	}
}

// timeoutResult is function run as separate goroutine, used to ensure timeout is correctly applied
// for every processTask() goroutine running
func timeoutResult(id uuid.UUID, start time.Time, d time.Duration, chDone chan struct{}, chResult chan<- Result) {
	// block further code execution for specified time duration
	<-time.After(d)

	select {
	// writing into Done channel will prevent further writing into chResult (disable **success** or **error** result)
	case chDone <- struct{}{}:
		// send **timeout** to resultHandler
		chResult <- Result{
			Id:    id,
			Start: start,
			Err:   errors.New("timeout: " + id.String()),
		}

	// chDone is buffered channel generated for each "task" goroutine
	// in order to prevent memory leaks it needs to be closed and "drained"
	default:
		close(chDone)
		<-chDone
		chDone = nil
	}
}

// errorResult should be called on every error captured during any processTask() DB operation (command)
func errorResult(id uuid.UUID, start time.Time, err error, chDone chan struct{}, chResult chan<- Result) {
	select {
	// writing into Done channel will prevent further writing into chResult (disable **success** or **timeout** result)
	case chDone <- struct{}{}:
		// send **error** to resultHandler
		chResult <- Result{
			Id:    id,
			Start: start,
			Err:   err,
		}

	// chDone is buffered channel generated for each "task" goroutine
	// in order to prevent memory leaks it needs to be closed and "drained"
	default:
		close(chDone)
		<-chDone
		chDone = nil
	}
}

// resultHandler is goroutine used to handle result of performed task
// it handles **success** & **error** results (**timeout** is treated as error)
func resultHandler(timeout time.Duration, chConcurrent chan struct{}, chResult chan Result, logger *zap.SugaredLogger) {
	for {
		// this line blocks until read from chResult is enabled
		result := <-chResult
		td := time.Now().Sub(result.Start).Milliseconds()
		// log success
		if result.Success {
			logger.Infow("success", "uuid", result.Id, "time", td)
			continue
		}

		logger.Warnw("error", "uuid", result.Id, "time", td, "err", result.Err)

		id := uuid.New()
		start := time.Now()

		// chDone is buffered channel generated for each "task" goroutine
		// in order to prevent memory leaks it needs to be closed and "drained"
		chDone := make(chan struct{}, 1)

		// **Important:** DB command used to replace old uuid (result.Id) with the new one (id)
		// should be executed here

		// repeat task
		go processTask(id, start, chConcurrent, chDone, chResult)
		go timeoutResult(id, start, timeout, chDone, chResult)
	}
}
