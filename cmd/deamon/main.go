package main

import (
	"gitlab.com/divilla/exporter/internal/config"
	"gitlab.com/divilla/exporter/internal/runner"
	"go.uber.org/zap"
)

//promise, future, async/await, callback, observable

func main() {
	cfg := config.NewConfig("config/config.yaml")
	logger, _ := zap.NewProduction()
	defer logger.Sync() // flushes buffer, if any
	sugar := logger.Sugar()

	runner.Run(cfg, sugar)
}
