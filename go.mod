module gitlab.com/divilla/exporter

go 1.18

require (
	github.com/google/uuid v1.3.0 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	go.uber.org/zap v1.21.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
